import {
    LoggingEvent
} from 'log4js';

import {
    CustomLayoutConfiguration,
    ISelector,
    ISelectorConfiguration,
    LayoutConfiguration,
    SelectorFactoryFunc
} from '@apexlearning/log4js-node-selectorlayout-selectortypings';

/**
 * log4js-node JSON configuration object for an {ISelectorLayout}.
 */
export interface ISelectorLayoutConfiguration extends CustomLayoutConfiguration {
    type: 'selector';

    /**
     * The default layout configuration to use when formatting a message if no selectors match.
     */
    defaultLayout: LayoutConfiguration;

    /**
     * A collection of selectors to match against messages.
     * Selectors will be evaluated for matches in the order defined here.
     */
    selectors: ISelectorConfiguration[];
}

/**
 * Named type for how log4js-node layouts transform a LoggingEvent into a string.
 */
export type LayoutFormatterFunc = (loggingEvent: LoggingEvent) => string;

/**
 * A function type suitable for passing to log4js-node.addLayout.
 * @param config Will be the JSON configuration object for the layout being registered
 */
export type LayoutRegistrarFunc = (config: any) => (LayoutFormatterFunc);

/**
 * Convenience type to create a {LayoutRegistrarFunc} for registering a selector layout which handles a given set of {ISelector}s.
 */
export type SelectorLayoutRegistrarFunc = (selectorRegistrations: { [type: string]: SelectorFactoryFunc }) => LayoutRegistrarFunc;

/**
 * Defines the contract for a selector layout.
 * Like most other log4js-node layouts, the minimal contract is a method which takes a {LoggingEvent} and returns a formatted string.
 */
export interface ISelectorLayout {
    format: LayoutFormatterFunc;
}

/*
 * To dynamically construct layouts, we have to delve into the log4js library a bit.
 */
// tslint:disable-next-line
const log4jsLayouts: any = require('log4js/lib/layouts');

/**
 * A default {ISelector} which matches all {LoggingEvent}s.
 */
export class DefaultSelector implements ISelector {
    private layout: LayoutConfiguration;

    constructor(layout: LayoutConfiguration) {
        this.layout = layout;
    }

    match(loggingEvent: LoggingEvent): boolean {
        return true;
    }

    getLayout(): LayoutConfiguration {
        return this.layout;
    }
}

/**
 * Internal type used to map a {ISelector} to the log4js-node {LayoutFormatterFunc} associated with that {ISelector}'s {Layout}.
 */
interface ISelectorFormatter {
    selector: ISelector;
    format: LayoutFormatterFunc;
}

/**
 * Default implementation of {ISelectorLayout}.
 */
export class SelectorLayout implements ISelectorLayout {
    /**
     * Looks up {LayoutFormatterFunc}s currently registered with log4js-node and associates them with a provided
     * {ISelector}.
     * @param {ISelector} selector
     * @returns {ISelectorFormatter}
     * @private
     */
    private static _createFormatter(selector: ISelector): ISelectorFormatter {
        const layout = selector.getLayout();
        return {
            selector: selector,
            format: <LayoutFormatterFunc> log4jsLayouts.layout(layout.type, layout)
        };
    }

    private defaultFormatter: ISelectorFormatter;
    private formatters: ISelectorFormatter[];

    constructor(config: ISelectorLayoutConfiguration, selectorRegistrations: { [type: string]: SelectorFactoryFunc }) {
        const defaultPatternSelector = new DefaultSelector(config.defaultLayout);
        this.defaultFormatter = SelectorLayout._createFormatter(defaultPatternSelector);

        this.formatters = config.selectors.map((selectorConfig: ISelectorConfiguration) => {
            const selector = selectorRegistrations[selectorConfig.type](selectorConfig);
            return SelectorLayout._createFormatter(selector);
        });
    }

    format: LayoutFormatterFunc = (loggingEvent: LoggingEvent): string => {
        for (let formatter of this.formatters) {
            if (formatter.selector.match(loggingEvent)) {
                return formatter.format(loggingEvent);
            }
        }

        return this.defaultFormatter.format(loggingEvent);
    }
}

/**
 * Utility implementation of a {SelectorLayoutRegistrarFunc} which constructs a {SelectorLayout}.
 * @param {{[p: string]: SelectorFactoryFunc}} selectorRegistrations
 *  A mapping of {ISelectorConfiguration.type} to {SelectorFactoryFunc}s which can build that {ISelector}
 * @returns {(config: any) => LayoutFormatterFunc}
 */
export const selectorLayoutRegistrar: SelectorLayoutRegistrarFunc =
    (selectorRegistrations: { [type: string]: SelectorFactoryFunc }) => {
        return (config: any) => {
            return new SelectorLayout(<ISelectorLayoutConfiguration> config, selectorRegistrations).format;
        };
    };
