import {
    SelectorLayout,
    ISelectorLayoutConfiguration
} from './log4js-node-selectorlayout';
import {
    LoggingEvent,
    PatternLayout,
    BaseLayout,
    DummyLayout,
    Level
} from 'log4js';
import { ISelector, ISelectorConfiguration } from '@apexlearning/log4js-node-selectorlayout-selectortypings';

describe('log4js-node-selectorlayout', () => {
    const defaultPatternLayout: PatternLayout = {
        type: 'pattern',
        pattern: 'HELLO: %m'
    };

    const neverMatchSelectorType = 'notreal';
    const neverMatchSelectorLayout: BaseLayout = {
        type: 'basic'
    };
    const neverMatchSelector: ISelector = {
        match: (loggingEvent: LoggingEvent) => {
            return false;
        },
        getLayout: () => {
            return neverMatchSelectorLayout;
        }
    };

    const matchingSelectorType = 'matching';
    const matchingSelectorLayout: DummyLayout = {
        type: 'dummy'
    };

    function createMatchingSelector(selectorConfig: ISelectorConfiguration): ISelector {
        return {
            match: (loggingEvent: LoggingEvent) => {
                return true;
            },
            getLayout: () => {
                return selectorConfig.layout;
            }
        };
    }

    it('Should return string matching default pattern if no selectors defined', () => {
        const config: ISelectorLayoutConfiguration = {
            type: 'selector',
            defaultLayout: defaultPatternLayout,
            selectors: []
        };
        const psl = new SelectorLayout(config, {});

        const logEvent: LoggingEvent = {
            categoryName: 'default',
            level: new MockLevel(),
            data: ['message'],
            startTime: new Date(),
            pid: 1,
            context: {}
        };

        const result = psl.format(logEvent);
        expect(result).toEqual(`HELLO: ${logEvent.data[0]}`);
    });

    it('Should return string matching default pattern if no selectors match', () => {
        const config: ISelectorLayoutConfiguration = {
            type: 'selector',
            defaultLayout: defaultPatternLayout,
            selectors: [
                {
                    type: neverMatchSelectorType,
                    layout: neverMatchSelectorLayout
                }
            ]
        };

        const psl = new SelectorLayout(config, { [neverMatchSelectorType]: () => neverMatchSelector });

        const logEvent: LoggingEvent = {
            categoryName: 'default',
            level: new MockLevel(),
            data: ['message'],
            startTime: new Date(),
            pid: 1,
            context: {}
        };

        const result = psl.format(logEvent);
        expect(result).toEqual(`HELLO: ${logEvent.data[0]}`);
    });

    it('should return string formatted to first matching selector', () => {
        const otherMatchingSelectorPatternLayout: PatternLayout = {
            type: 'pattern',
            pattern: 'MATCHING, BUT NOT WHAT WE WANT'
        };
        const config: ISelectorLayoutConfiguration = {
            type: 'selector',
            defaultLayout: defaultPatternLayout,
            selectors: [
                {
                    type: neverMatchSelectorType,
                    layout: neverMatchSelectorLayout
                },
                {
                    type: matchingSelectorType,
                    layout: matchingSelectorLayout
                },
                {
                    type: 'matching2',
                    layout: otherMatchingSelectorPatternLayout
                }
            ]
        };

        const selectorRegistrations = {
            [neverMatchSelectorType]: () => neverMatchSelector,
            [matchingSelectorType]: createMatchingSelector,
            matching2: createMatchingSelector
        };
        const psl = new SelectorLayout(config, selectorRegistrations);

        const logEvent: LoggingEvent = {
            categoryName: 'default',
            level: new MockLevel(),
            data: ['message'],
            startTime: new Date(),
            pid: 1,
            context: {}
        };

        const result = psl.format(logEvent);
        expect(result).toEqual(`${logEvent.data[0]}`);
    });

    describe('kinds of layouts', () => {
        it('should be capable of using a pattern layout', () => {
            const matchingSelectorPatternLayout: PatternLayout = {
                type: 'pattern',
                pattern: 'PATTERN %m'
            };
            const config: ISelectorLayoutConfiguration = {
                type: 'selector',
                defaultLayout: defaultPatternLayout,
                selectors: [
                    {
                        type: neverMatchSelectorType,
                        layout: neverMatchSelectorLayout
                    },
                    {
                        type: matchingSelectorType,
                        layout: matchingSelectorPatternLayout
                    }
                ]
            };

            const selectorRegistrations = {
                [neverMatchSelectorType]: () => neverMatchSelector,
                [matchingSelectorType]: createMatchingSelector
            };
            const psl = new SelectorLayout(config, selectorRegistrations);

            const logEvent: LoggingEvent = {
                categoryName: 'default',
                level: new MockLevel(),
                data: ['message'],
                startTime: new Date(),
                pid: 1,
                context: {}
            };

            const result = psl.format(logEvent);
            expect(result).toEqual(`PATTERN ${logEvent.data[0]}`);
        });

        it('should be capable of using a dummy layout', () => {
            const config: ISelectorLayoutConfiguration = {
                type: 'selector',
                defaultLayout: defaultPatternLayout,
                selectors: [
                    {
                        type: matchingSelectorType,
                        layout: matchingSelectorLayout
                    }
                ]
            };

            const selectorRegistrations = {
                [matchingSelectorType]: createMatchingSelector
            };
            const psl = new SelectorLayout(config, selectorRegistrations);

            const logEvent: LoggingEvent = {
                categoryName: 'default',
                level: new MockLevel(),
                data: ['message'],
                startTime: new Date(),
                pid: 1,
                context: {}
            };

            const result = psl.format(logEvent);
            expect(result).toEqual(`${logEvent.data[0]}`);
        });
    });
});

class MockLevel implements Level {
    isEqualTo(other: string): boolean;
    isEqualTo(otherLevel: Level): boolean;
    isEqualTo(other: string | Level): boolean {
        return false;
    }

    isGreaterThanOrEqualTo(other: string): boolean;
    isGreaterThanOrEqualTo(otherLevel: Level): boolean;
    isGreaterThanOrEqualTo(other: string | Level): boolean {
        return false;
    }

    isLessThanOrEqualTo(other: string): boolean;
    isLessThanOrEqualTo(otherLevel: Level): boolean;
    isLessThanOrEqualTo(other: string | Level): boolean {
        return false;
    }
}