# log4js-node-selectorlayout

An extension to [log4js-node][1] to log messages to one of multiple different
layouts by matching it against selector functions. It's basically the
log4js-node equivalent of [log4j Pattern Selectors][3].

When you use the selector layout, whenever you log a message with log4js-node,
the message will be matched against a list of selectors. These selectors are
essentially functions, which you can define yourself or import from packages
like [log4js-node-marker][2], which examine a message and determine if the selector matches the
message. If the selector matches the message, the message will be logged using a
layout associated with that selector (which could be another selector layout).

This allows you finer control over the format of your log messages.

## Usage

Install the package as a dependency of your project

    npm install --save log4js-node-selectorlayout

Then, add the selector layout configuration to your log4js-node configuration JSON.
Here's an example using the MarkerSelector from [log4js-node-marker][2]. In this example, we want
to log messages with a particular pattern by default, but if the message was logged
with a special marker named `JSON`, we assume the message data is a JSON object
and we only want to log that JSON rather than apply the pattern.

    {
      "options": {
        "categories": {
          "default": {
            "appenders": [ "console" ],
            "level": "info"
          }
        },
        "appenders": {
          "console": {
            "type": "console",
            "layout": {
              "type": "selector",
              "defaultLayout": {
                "type": "pattern",
                "pattern": "%[[%d] [%p] [%c]%] - %m"
              },
              "selectors": [
                {
                  "type": "marker",
                  "matchMarkerName": "JSON",
                  "layout": {
                    "type": "pattern",
                    "pattern": "%m"
                  }
                }
              ]
            }
          }
        }
      }
    }

Next, register the layout selector when you instantiate log4js-node in your application.
For example, again using the MarkerSelector from [log4js-node-marker][2]:

    const selectorRegistrations = {
        'marker': markerSelectorFactory()
    };
    log4js.addLayout('selector', selectorLayoutRegistrar(selectorRegistrations));
    log4js.configure(log4jsConfigObject);

Now use log4js-node like you normally would.

## License Information

Copyright 2018 Apex Learning Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: https://github.com/log4js-node/log4js-node/
[2]: https://bitbucket.org/apexlearning/log4js-node-marker/
[3]: https://logging.apache.org/log4j/2.x/manual/layouts.html#Pattern_Selectors