# log4js-node-selectorlayout-selectortypings

A lightweight typings package that covers selectors and their configuration from
[log4js-node-selectorlayout][1] so that other packages can define selectors without
requiring the whole selector layout package as a dependency.

## Usage

Install the package as a dependency of your project

    npm install --save log4js-node-selectorlayout-selectortypings

Now the typings are available for you to reference and re-export in your own
project.

## License Information

Copyright 2018 Apex Learning Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: https://bitbucket.org/apexlearning/log4js-node-selectorlayout/
