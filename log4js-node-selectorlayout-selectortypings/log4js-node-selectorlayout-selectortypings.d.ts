import { CustomLayout, Layout, LoggingEvent } from 'log4js';

/**
 * The log4js-node {Layout} types really represents the configuration of a layout, not the programmatic contract of a layout.
 * Provide a more clear name.
 */
export type LayoutConfiguration = Layout;

/**
 * The log4js-node {Layout} types really represents the configuration of a layout, not the programmatic contract of a layout.
 * Provide a more clear name.
 */
export type CustomLayoutConfiguration = CustomLayout;

/**
 * log4js-node base JSON configuration object for an {ISelector}.
 * Can be extended with arbitrary properties to support custom selectors.
 */
export interface ISelectorConfiguration {
    [key: string]: any;

    /**
     * The type of the selector.
     * Must match a value provided as input to the {SelectorLayoutRegistrarFunc} when registring the selector layout.
     */
    type: string;

    /**
     * The {LayoutConfiguration} associated with this {ISelector}
     */
    layout: LayoutConfiguration;
}

/**
 * A selector examines an individual {LoggingEvent} and determines if it matches the selector's criteria.
 * If it does, the selector will format the logging event according to a configured {LayoutConfiguration}.
 */
export interface ISelector {
    /**
     * Does this selector match the provided {LoggingEvent}?
     * @param {LoggingEvent} loggingEvent
     * @returns {boolean}
     */
    match(loggingEvent: LoggingEvent): boolean;

    /**
     * The {LayoutConfiguration} associated with this selector.
     * @returns {LayoutConfiguration}
     */
    getLayout(): LayoutConfiguration;
}

/**
 * Factory function defining how to create a {ISelector} from a given {ISelectorConfiguration}.
 */
export type SelectorFactoryFunc = (selectorConfig: ISelectorConfiguration) => ISelector;
