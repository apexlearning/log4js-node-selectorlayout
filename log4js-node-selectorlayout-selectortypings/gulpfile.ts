import * as gulp from 'gulp';

const ts = require('gulp-typescript');
const tslint = require('gulp-tslint');
const tsProject = ts.createProject('tsconfig.json');

const EOL = require('os').EOL;
const fs = require('fs');

gulp.task('lint', () => {
    return tsProject.src()
        .pipe(tslint({
            formatter: 'verbose'
        }))
        .pipe(tslint.report({
            emitError: true
        }));
});

gulp.task('dist', () => {
    const npmrcContents = [
        'registry=https://registry.npmjs.org/',
        '//registry.npmjs.org/:_authToken=${NPM_TOKEN}',
        ''
    ].join(EOL);

    fs.writeFileSync('.npmrc', npmrcContents);
});
