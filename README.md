# log4js-node-selectorlayout

An extension to [log4js-node][1] to log messages to one of multiple different
layouts by matching it against selector functions. It's basically the
log4js-node equivalent of [log4j Pattern Selectors][2].

This repository contains the source code for two packages:

* `log4js-node-selectorlayout`: contains the actual implementation of the selector layout.
Use this package if you actually want to use the selector layout in your project.

* `log4js-node-selectorlayout-selectortypings`: contains just typings for selectors and
their configuration. Use this package if you want to develop your own selectors without
requiring the whole selector layout package as a dependency.

[1]: https://github.com/log4js-node/log4js-node/
[2]: https://logging.apache.org/log4j/2.x/manual/layouts.html#Pattern_Selectors
